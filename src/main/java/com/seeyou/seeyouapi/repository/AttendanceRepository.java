package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Attendance;
import com.seeyou.seeyouapi.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    Page<Attendance> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
    Page<Attendance> findAllByIdGreaterThanEqualAndMemberOrderByIdDesc(long id, Member member, Pageable pageable);
    Optional<Attendance> findByAttendanceYearAndAttendanceMonthAndAttendanceDayAndMember(int year, int month, int day, Member member);
    List<Attendance> findAllByAttendanceYearAndAttendanceMonthAndMember(int year, int month, Member member);
}