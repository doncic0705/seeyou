package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Basket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BasketRepository extends JpaRepository<Basket, Long> {
}
