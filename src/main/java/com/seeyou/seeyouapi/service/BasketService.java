package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Basket;
import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.model.basket.BasketItem;
import com.seeyou.seeyouapi.model.basket.BasketRequest;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.repository.BasketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BasketService {
    private final BasketRepository basketRepository;

    public void setBasket(Product product, BasketRequest request) {
        Basket basket = new Basket.Builder(product, request).build();
        basketRepository.save(basket);
    }

    public ListResult<BasketItem> getList() {
        List<Basket> originList = basketRepository.findAll();

        List<BasketItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new BasketItem.Builder(e).build()));

        return ListConvertService.settingResult(result);
    }

    public void delBasket() {
        basketRepository.deleteAll();
    }
}
