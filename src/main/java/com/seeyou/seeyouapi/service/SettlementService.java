package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Settlement;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.settlement.SettlementItem;
import com.seeyou.seeyouapi.model.settlement.SettlementResponse;
import com.seeyou.seeyouapi.repository.ProductOrderRepository;
import com.seeyou.seeyouapi.repository.SettlementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class SettlementService {
    private final SettlementRepository settlementRepository;
    private final ProductOrderRepository productOrderRepository;

    public void setSettlement(BigDecimal price, int year, String month) {
        Settlement settlement = new Settlement.Builder(price, year, month).build();
        settlementRepository.save(settlement);
    }

//    public void setSettlement(SettlementRequest request) {
//        List<ProductOrder> originList = productOrderRepository.findAllByOrderYearAndOrderMonthAndIsComplete(request.getSettlementYear(), request.getSettlementMonth().toString(), true);
//        BigDecimal price = BigDecimal.valueOf(0);
//        for (ProductOrder productOrder : originList) {
//            price = price.add(productOrder.getPrice());
//        }
//        Settlement settlement = new Settlement.Builder(price, request).build();
//        settlementRepository.save(settlement);
//    }
    public ListResult<SettlementItem> getList(int page) {
        Page<Settlement> originList = settlementRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<SettlementItem> result = new LinkedList<>();
        for (Settlement settlement : originList) {
            result.add(new SettlementItem.Builder(settlement).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public SettlementResponse getSettlement(int year, String month) {
        List<Settlement> originList = settlementRepository.findAllBySettlementYearAndSettlementMonth(year, month);
        BigDecimal price = BigDecimal.valueOf(0);
        for (Settlement settlement : originList) {
            price = price.add(settlement.getPrice());
        }
        return new SettlementResponse.Builder(price).build();
    }

    public void putSettlement(int year, String month) {
        List<Settlement> originList = settlementRepository.findAllBySettlementYearAndSettlementMonth(year, month);
        for (Settlement settlement : originList) {
            settlement.putIsComplete();
            settlementRepository.save(settlement);
        }
    }

//    public void putSettlement(long id) {
//        Settlement settlement = settlementRepository.findById(id).orElseThrow(CMissingDataException::new);
//        settlement.putIsComplete();
//        settlementRepository.save(settlement);
//    }
}