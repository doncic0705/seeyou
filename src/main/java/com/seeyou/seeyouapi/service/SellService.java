package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Sell;
import com.seeyou.seeyouapi.entity.SellProduct;
import com.seeyou.seeyouapi.lib.CommonFile;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.sell.SellItem;
import com.seeyou.seeyouapi.model.sell.SellRefundRequest;
import com.seeyou.seeyouapi.model.sell.SellRequest;
import com.seeyou.seeyouapi.repository.SellProductRepository;
import com.seeyou.seeyouapi.repository.SellRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SellService {
    private final SellRepository sellRepository;
    private final SellProductRepository sellProductRepository;

    public void setSells(MultipartFile csvFile) throws IOException {
        File file = CommonFile.multipartToFile(csvFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line = "";
        int index = 0; // 줄 번호
        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {

                String[] cols = line.split(",");

                SellProduct product = sellProductRepository.findById(Long.parseLong(cols[1])).orElseThrow();

                SellRequest sellRequest = new SellRequest();
                sellRequest.setBillNumber(Long.parseLong(cols[0]));
                sellRequest.setQuantity(Integer.parseInt(cols[2]));
                sellRequest.setPrice(Double.parseDouble(cols[3]));

                Sell sell = new Sell.Builder(product, sellRequest).build();
                sellRepository.save(sell); // 저장
            }
            index++; // index = index + 1;
        }
        bufferedReader.close();
    }

    // 판매 내역
    public ListResult<SellItem> sellPageItemList (int page) {
        Page<Sell> originData = sellRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<SellItem> result = new LinkedList<>();
        originData.forEach(e -> result.add(new SellItem.Builder(e).build()));
        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }

    // 판매 내역 환불 처리
    public void putIsCompleteUpdate(SellRefundRequest request) {
        List<Sell> originData = sellRepository.findAllByBillNumber(request.getBillNumber());

        for (Sell e : originData) {
            e.putIsCompleteUpdate();
            sellRepository.save(e);
        }
    }

    public ListResult<SellItem> getBillNumberDetailList(long billNumber) {
        List<Sell> originData = sellRepository.findAllByBillNumber(billNumber);
        List<SellItem> result = new LinkedList<>();

        originData.forEach(e -> result.add(new SellItem.Builder(e).build()));

        return ListConvertService.settingResult(result);
    }

}
