package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.model.attendance.AttendanceSearchRequest;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.money.MoneyHistoryItem;
import com.seeyou.seeyouapi.model.money.MoneyHistoryRequest;
import com.seeyou.seeyouapi.model.money.MoneyHistoryResponse;
import com.seeyou.seeyouapi.model.money.MoneyHistorySearchRequest;
import com.seeyou.seeyouapi.service.MemberDataService;
import com.seeyou.seeyouapi.service.MoneyHistoryService;
import com.seeyou.seeyouapi.service.ProfileService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "급여 지급 내역")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/money-history")
public class MoneyHistoryController {
    private final MoneyHistoryService moneyHistoryService;
    private final MemberDataService memberDataService;
    private final ProfileService profileService;

    @ApiOperation(value = "급여 지급 내역 등록")
    @PostMapping("/member-id/{memberId}")
    public CommonResult setMoneyHistory(@PathVariable long memberId, @RequestBody @Valid MoneyHistoryRequest moneyHistoryRequest) {
        Member member = memberDataService.getMemberData(memberId);
        moneyHistoryService.setMoneyDetails(member, moneyHistoryRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "급여 지급 내역 리스트")
    @GetMapping("/histories/{page}")
    public ListResult<MoneyHistoryItem> getMoneyHistories(
            @PathVariable int page,
            @RequestParam(value = "searchYear", required = false) String searchYear,
            @RequestParam(value = "searchMonth", required = false) String searchMonth
            ) {
        MoneyHistorySearchRequest request = new MoneyHistorySearchRequest();
        request.setMoneyYear(searchYear);
        request.setMoneyMonth(searchMonth);
        return ResponseService.getListResult(moneyHistoryService.getMoneyHistories(page, request), true);
}

    @ApiOperation(value = "급여 지급 내역 리스트 상세")
    @GetMapping("/history")
    public SingleResult<MoneyHistoryResponse> getMoneyHistory(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(moneyHistoryService.getMoneyHistory(id));
    }

    @ApiOperation(value = "나의 급여 지급 내역 리스트")
    @GetMapping("/my-histories")
    public ListResult<MoneyHistoryItem> getMyMoneyHistories() {
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(moneyHistoryService.getMyMoneyHistories(member), true);
    }

    @ApiOperation(value = "나의 급여 지급 내역 리스트")
    @GetMapping("/my-history")
    public SingleResult<MoneyHistoryResponse> getMyMoneyHistory(@RequestParam("id") long id) {
        Member member = profileService.getMemberData();
        return ResponseService.getSingleResult(moneyHistoryService.getMyMoneyHistory(id, member));
    }
}
