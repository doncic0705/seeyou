package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.sell.SellItem;
import com.seeyou.seeyouapi.model.sell.SellRefundRequest;
import com.seeyou.seeyouapi.service.ResponseService;
import com.seeyou.seeyouapi.service.SellService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Api(tags = "판매 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sell")
public class SellController {
    private final SellService sellService;

    @ApiOperation(value = "판매 등록")
    @PostMapping("/file-upload")
    public CommonResult setSells(@RequestParam("csvFile") MultipartFile csvFile) throws Exception {
        sellService.setSells(csvFile);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "판매 리스트")
    @GetMapping("/list")
    public ListResult<SellItem> getSellList(@RequestParam(value = "page", defaultValue = "1") int page) {
        return ResponseService.getListResult(sellService.sellPageItemList(page), true);
    }

    @ApiOperation(value = "영수증 상세 정보")
    @GetMapping("/billNumber-detail")
    public ListResult<SellItem> getBillNumberDetailList(@RequestParam(value = "billNumber") long billNumber) {
        return ResponseService.getListResult(sellService.getBillNumberDetailList(billNumber), true);
    }

    @ApiOperation(value = "상품 환불")
    @PutMapping("/refund")
    public CommonResult putIsCompleteUpdate(@RequestBody @Valid SellRefundRequest request) {
        sellService.putIsCompleteUpdate(request);
        return ResponseService.getSuccessResult();
    }
}
