package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderCompleteItem;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderItem;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderResponse;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderRequest;
import com.seeyou.seeyouapi.service.*;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "발주 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product-order")
public class ProductOrderController {
    private final ProductOrderService productOrderService;
    private final StockService stockService;
    private final ProductService productService;
    private final SettlementService settlementService;
    private final BasketService basketService;

    @ApiOperation(value = "발주 등록")
    @PostMapping("/data")
    public CommonResult setProductOrder(@RequestBody @Valid ProductOrderRequest request, long productId) {
        Product product = productService.getProductData(productId);
        productOrderService.setProductOrder(product, request);
        basketService.delBasket();
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "발주 리스트")
    @GetMapping("/list")
    public ListResult<ProductOrderItem> getProductOrderList(@RequestParam(value = "page", defaultValue = "1") int page) {
        return ResponseService.getListResult(productOrderService.getProductOrderList(page), true);
    }

    @ApiOperation(value = "발주 리스트 상세")
    @GetMapping("/order")
    public SingleResult<ProductOrderResponse> getProductOrder(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(productOrderService.getProductOrder(id));
    }

    @ApiOperation(value = "발주 상태 수정")
    @PutMapping("/complete/{id}")
    public CommonResult putProductOrderIsComplete(@PathVariable long id) {
        productOrderService.putProductOrder(id);
        ProductOrder productOrder = productOrderService.getProductData(id);
        stockService.putAutoUpdateStock(productOrder);
        settlementService.setSettlement(productOrder.getPrice(), productOrder.getOrderYear(), productOrder.getOrderMonth());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "완료된 발주 리스트")
    @GetMapping("/complete-list")
    public ListResult<ProductOrderCompleteItem> getProductCompleteOrderPageList(@RequestParam(value = "page", defaultValue = "1") int page) {
        return ResponseService.getListResult(productOrderService.getCompleteProductPageList(page), true);
    }
}
