package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.money.MoneyItem;
import com.seeyou.seeyouapi.model.money.MoneyRequest;
import com.seeyou.seeyouapi.model.money.MoneyResponse;
import com.seeyou.seeyouapi.service.MemberDataService;
import com.seeyou.seeyouapi.service.MoneyService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "급여 내역")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/money")
public class MoneyController {
    private final MoneyService moneyService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "급여 등록")
    @PostMapping("/member-id/{memberId}")
    public CommonResult setMoney(@PathVariable long memberId, @RequestBody @Valid MoneyRequest moneyRequest) {
        Member member = memberDataService.getMemberData(memberId); // service에서 엿바꿔주는 친구를 만든걸 불러온다.
        moneyService.setMoney(member, moneyRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "급여 리스트")
    @GetMapping("/all")
    public ListResult<MoneyItem> getMoneyList(@RequestParam (value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(moneyService.getMoneyList(page), true);
    }
    @ApiOperation(value = "급여 리스트 상세")
    @GetMapping("/alls")
    public SingleResult<MoneyResponse> getMoneyList(@RequestParam(value = "id") long id) {
        return ResponseService.getSingleResult(moneyService.getMoneyLists(id));
    }
    @ApiOperation(value = "급여 수정")
    @PutMapping("/{id}")
    public CommonResult putMoney(@PathVariable long id, @RequestBody @Valid MoneyRequest moneyRequest) {
        moneyService.putMoney(id, moneyRequest);
        return ResponseService.getSuccessResult();
    }
}
