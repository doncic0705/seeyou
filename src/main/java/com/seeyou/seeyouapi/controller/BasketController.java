package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.model.basket.BasketItem;
import com.seeyou.seeyouapi.model.basket.BasketRequest;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.service.BasketService;
import com.seeyou.seeyouapi.service.ListConvertService;
import com.seeyou.seeyouapi.service.ProductService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "장바구니 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/basket")
public class BasketController {
    private final BasketService basketService;
    private final ProductService productService;

    @ApiOperation(value = "장바구니 등록")
    @PostMapping("/product-id/{productId}")
    public CommonResult setBasket(@PathVariable long productId, @RequestBody @Valid BasketRequest request) {
        Product product = productService.getProductData(productId);
        basketService.setBasket(product, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "장바구니 리스트")
    @GetMapping("/product-id/list")
    public ListResult<BasketItem> getList(){
        return ResponseService.getListResult(basketService.getList(), true);
    }
}
