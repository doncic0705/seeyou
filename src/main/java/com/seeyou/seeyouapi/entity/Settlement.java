package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import com.seeyou.seeyouapi.model.settlement.SettlementRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Settlement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer settlementYear;

    @Column(nullable = false)
    private String settlementMonth;

    @Column(nullable = false)
    private BigDecimal price;

    @Column(nullable = false)
    private Boolean isComplete;

    public void putIsComplete() {
        this.isComplete = true;
    }

    private Settlement(Builder builder) {
        this.settlementYear = builder.settlementYear;
        this.settlementMonth = builder.settlementMonth;
        this.price = builder.price;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<Settlement> {
        private final Integer settlementYear;
        private final String settlementMonth;
        private final BigDecimal price;
        private final Boolean isComplete;

        public Builder(BigDecimal price, int year, String month) {
            this.settlementYear = year;
            this.settlementMonth = month;
            this.price = price;
            this.isComplete = false;
        }

        @Override
        public Settlement build() {
            return new Settlement(this);
        }
    }
}
