package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.enums.MemberGroup;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.model.member.MemberCreateRequest;
import com.seeyou.seeyouapi.model.member.MemberUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    @Column(nullable = false, length = 13)
    private String contact;

    @Column(nullable = false, length = 20)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private LocalDate dateCreate;


    private LocalDate dateEnd;

    @Column(nullable = false)
    private Boolean isEnabled;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public void putPassword(String password) {this.password = password;}

    public void putIsEnabled() {
        this.isEnabled = false;
        this.dateEnd = LocalDate.now();
    }

    public void putMember(MemberUpdateRequest updateRequest) {
        this.memberName = updateRequest.getMemberName();
        this.memberGroup = updateRequest.getMemberGroup();
        this.contact = updateRequest.getContact();
    }

    private Member(Builder builder) {
        this.memberName = builder.memberName;
        this.memberGroup = builder.memberGroup;
        this.contact = builder.contact;
        this.username = builder.username;
        this.password = builder.password;
        this.dateCreate = builder.dateCreate;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<Member> {

        private final String memberName;
        private final MemberGroup memberGroup;
        private final String contact;
        private final String username;
        private final String password;
        private final LocalDate dateCreate;
        private final Boolean isEnabled;

        public Builder(MemberGroup memberGroup, MemberCreateRequest createRequest) {
            this.memberName = createRequest.getMemberName();
            this.memberGroup = memberGroup;
            this.contact = createRequest.getContact();
            this.username = createRequest.getUsername();
            this.password = createRequest.getPassword();
            this.dateCreate = LocalDate.now();
            this.isEnabled = true;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}