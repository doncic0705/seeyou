package com.seeyou.seeyouapi.model.money;

import com.seeyou.seeyouapi.entity.MoneyHistory;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistoryItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "직원명")
    private String member;

    @ApiModelProperty(notes = "연도")
    private Integer moneyYear;

    @ApiModelProperty(notes = "월")
    private String moneyMonth;

    @ApiModelProperty(notes = "급여")
    private BigDecimal money;

    private MoneyHistoryItem(Builder builder) {
        this.id = builder.id;
        this.member = builder.member;
        this.moneyYear = builder.moneyYear;
        this.moneyMonth = builder.moneyMonth;
        this.money = builder.money;

    }

    public static class Builder implements CommonModelBuilder<MoneyHistoryItem> {
        private final Long id;
        private final String member;
        private final Integer moneyYear;
        private final String moneyMonth;
        private final BigDecimal money;

        public Builder(MoneyHistory moneyHistory) {
            this.id = moneyHistory.getId();
            this.member = moneyHistory.getMember().getMemberName();
            this.moneyYear = moneyHistory.getMoneyYear();
            this.moneyMonth = moneyHistory.getMoneyMonth();
            this.money = moneyHistory.getMoney();
        }
        @Override
        public MoneyHistoryItem build() {
            return new MoneyHistoryItem(this);
        }
    }
}
