package com.seeyou.seeyouapi.model.money;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MoneyHistorySearchRequest {
    private String moneyYear;
    private String moneyMonth;
}
