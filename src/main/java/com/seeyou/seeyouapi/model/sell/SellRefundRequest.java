package com.seeyou.seeyouapi.model.sell;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SellRefundRequest {
    @ApiModelProperty(notes = "영수증 번호")
    @NotNull
    private Long billNumber;
}
