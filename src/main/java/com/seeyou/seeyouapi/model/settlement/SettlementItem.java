package com.seeyou.seeyouapi.model.settlement;

import com.seeyou.seeyouapi.entity.Settlement;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SettlementItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "년월")
    private String dateSettlement;

    @ApiModelProperty(notes = "금액")
    private BigDecimal price;

    @ApiModelProperty(notes = "완료여부")
    private String isComplete;

    private SettlementItem(Builder builder) {
        this.id = builder.id;
        this.dateSettlement = builder.dateSettlement;
        this.price = builder.price;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<SettlementItem> {
        private final Long id;
        private final String dateSettlement;
        private final BigDecimal price;
        private final String isComplete;

        public Builder(Settlement settlement) {
            this.id = settlement.getId();
            this.dateSettlement = settlement.getSettlementYear() + "-" + settlement.getSettlementMonth();
            this.price = settlement.getPrice();
            this.isComplete = settlement.getIsComplete() ? "O" : "X";
        }

        @Override
        public SettlementItem build() {
            return new SettlementItem(this);
        }
    }
}
