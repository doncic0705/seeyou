package com.seeyou.seeyouapi.model.income;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class IncomeRequest {
    @ApiModelProperty(notes = "연도", required = true)
    @NotNull
    private String incomeYear;

    @ApiModelProperty(notes = "월", required = true)
    @NotNull
    private String incomeMonth;

    @ApiModelProperty(notes = "지출항목(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String incomeCategory;

    @ApiModelProperty(notes = "금액", required = true)
    @NotNull
    @Min(value = 0)
    private Double price;
}
