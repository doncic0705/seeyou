package com.seeyou.seeyouapi.model.income;

import com.seeyou.seeyouapi.entity.Income;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class IncomeItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "연도")
    private String incomeYear;

    @ApiModelProperty(notes = "월")
    private String incomeMonth;

    @ApiModelProperty(notes = "지출항목")
    private String incomeCategory;

    @ApiModelProperty(notes = "금액")
    private BigDecimal price;

    private IncomeItem(Builder builder) {
        this.id = builder.id;
        this.incomeYear = builder.incomeYear;
        this.incomeMonth = builder.incomeMonth;
        this.incomeCategory = builder.incomeCategory;
        this.price = builder.price;
    }

    public static class Builder implements CommonModelBuilder<IncomeItem> {
        private final Long id;
        private final String incomeYear;
        private final String incomeMonth;
        private final String incomeCategory;
        private final BigDecimal price;

        public Builder(Income income) {
            this.id = income.getId();
            this.incomeYear = income.getIncomeYear();
            this.incomeMonth = income.getIncomeMonth();
            this.incomeCategory = income.getIncomeCategory();
            this.price = income.getPrice();
        }

        @Override
        public IncomeItem build() {
            return new IncomeItem(this);
        }
    }
}
