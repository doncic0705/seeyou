package com.seeyou.seeyouapi.model.productOrder;

import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrderItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "금액")
    private BigDecimal price;

    @ApiModelProperty(notes = "발주일")
    private String dateOrder;

    private ProductOrderItem(Builder builder) {
        this.id = builder.id;
        this.productName = builder.productName;
        this.price = builder.price;
        this.dateOrder = builder.dateOrder;
    }

    public static class Builder implements CommonModelBuilder<ProductOrderItem> {
        private final Long id;
        private final String productName;
        private final BigDecimal price;
        private final String dateOrder;

        public Builder(ProductOrder productOrder) {
            this.id = productOrder.getId();
            this.productName = productOrder.getProduct().getProductName();
            this.price = productOrder.getPrice();
            this.dateOrder = productOrder.getOrderYear() + "-" + productOrder.getOrderMonth() + "-" + productOrder.getOrderDay() + " " +  productOrder.getTimeOrder().getHour() + ":" + productOrder.getTimeOrder().getMinute();
        }

        @Override
        public ProductOrderItem build() {
            return new ProductOrderItem(this);
        }
    }
}
