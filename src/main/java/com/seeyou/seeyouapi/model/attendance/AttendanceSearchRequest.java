package com.seeyou.seeyouapi.model.attendance;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttendanceSearchRequest {
    private String attendanceYear;
    private String attendanceMonth;
    private String attendanceDay;
    private String name;
}
