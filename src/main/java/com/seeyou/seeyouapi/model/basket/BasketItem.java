package com.seeyou.seeyouapi.model.basket;
import com.seeyou.seeyouapi.entity.Basket;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BasketItem {
    private String product;

    private Integer quantity;

    private BigDecimal price;

    private BasketItem(Builder builder) {
        this.product = builder.product;
        this.quantity = builder.quantity;
        this.price = builder.price;
    }
    public static class Builder implements CommonModelBuilder<BasketItem> {
        private final String product;

        private final Integer quantity;

        private final BigDecimal price;

        public Builder(Basket basket) {
            this.product = basket.getProduct().getProductName();
            this.quantity = basket.getQuantity();
            this.price = basket.getPrice();
        }

        @Override
        public BasketItem build() {
            return new BasketItem(this);
        }
    }
}
