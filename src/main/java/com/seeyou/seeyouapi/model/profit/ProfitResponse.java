package com.seeyou.seeyouapi.model.profit;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProfitResponse {
    @ApiModelProperty(notes = "순수익")
    private BigDecimal profit;

    private ProfitResponse(Builder builder) {
        this.profit = builder.profit;
    }

    public static class Builder implements CommonModelBuilder<ProfitResponse> {
        private final BigDecimal profit;

        public Builder(BigDecimal profit) {
            this.profit = profit;
        }

        @Override
        public ProfitResponse build() {
            return new ProfitResponse(this);
        }
    }
}
